<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h1 class="grob">What is your data collection policy? What do you record?</h1>
      <h3>What is your data collection policy? What do you record?</h3>
      <p>We don't collect any personal information on our visitors.</p>
      <p>When you use iZSearch, we do not record your IP address, we do not record which browser you are using
        (Internet Explorer, Safari, Firefox, etc.), we do not record your computer platform (Windows, Mac,
        Linux, etc.), and we do not record the words or phrases you searched for.</p>
      <p>The only information we record is an aggregate total of how many searches are performed on our website
        each day (a measure of overall traffic), and we break down those overall traffic numbers by
        language.</p>
      <p>Our zero data-collection policy is important, because even seemingly harmless information can be
        combined to reveal more information than you might care to disclose.</p>
      <p>As a result, we never collect or store your IP address, browser, or platform information, because that
        data can be used to uniquely identify your computer or location -- or it can be combined with other data
        to learn even more about you. Your search terms can also convey personal information (think of someone
        entering a name together with a social security number) so we never collect or record those either.</p>
      <p>Please click this link to view our Privacy Policy.</p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
