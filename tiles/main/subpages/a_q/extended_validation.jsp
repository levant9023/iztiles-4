<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h1 class="grob">Does iZSearch have an Extended Validation (EV) SSL certificate?</h1>
      <h3>Does iZSearch have an Extended Validation (EV) SSL certificate?</h3>
      <p>Traditional SSL certificates provide basic information about the certificate holder, as well as the organization that issued the certificate. However, the amount of validation done to confirm the identify of the organization requesting the certificate varies considerably. To obtain an EV SSL certificate, the certificate holder must first go through a rigorous validation process to confirm itself as a legally-established business or organization with a verifiable identity.
        We are researching the best approach to replace our current www.izsearch.com certificates.
        To learn more about EV SSL standards and SSL technology, please visit <a href="https://en.wikipedia.org/wiki/Extended_Validation_Certificate.">https://en.wikipedia.org/wiki/Extended_Validation_Certificate.</a></p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
