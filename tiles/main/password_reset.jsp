<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:set var="req" value="${pageContext.request}" />
<c:set var='domain' value='${req.scheme}://${req.serverName}:${req.serverPort}'/>
<c:set var="baseURL" value="${domain}${req.contextPath}"/>

<html>
<head>
  <title>iZSearch Password Recovery</title>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
  <script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.js"></script>
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/mobile/js/vendors/bootstrap/css/bootstrap.min.css"/>
  <link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/subpages/login.css" />
  <link rel="stylesheet" type="text/css" href="${baseURL}/resources/font-awesome-4.3.0/css/font-awesome.min.css"/>

  <style>
    #wrapper .col-md-6,
    #wrapper .col-sm-6{
      margin: 0 auto;
      float: none;
    }
    #wrapper .btn-reset {
      background-color: #59B2E0;
      outline: none;
      color: #fff;
      font-size: 14px;
      height: auto;
      font-weight: normal;
      padding: 14px 0;
      text-transform: uppercase;
      border-color: #59B2E6;
    }
  </style>

</head>
<body>
  <div class="header">
    <a class="logo" href="${baseURL}/"><img src="${baseURL}/resources/img/logo.svg" alt="logo"><br>© iZSearch</a>
    <h1>iZSearch Account Recovery</h1>
  </div>

  <div id="wrapper" class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="panel panel-login">
          <div class="panel-heading">
            <div class="row">
              <div class="col-xs-12">
                <h2>Password Recovery</h2>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div name="password-reset-message"></div>
            <div class="row">
              <div class="col-lg-12">
                <form name="passwordRecovery" action="javascript://" method="post"  style="display: block;">
                  <div class="form-error text-danger message"></div>
                  <div class="form-group">
                    <input type="email" name="username" tabindex="1" class="form-control" placeholder="Email Address" value="">
                    <div name="username-error" class="text-danger message"></div>
                    <input type="hidden" name="psrv" value="psrvalid"/>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <button type="button" name="btn-password-reset" tabindex="4" class="form-control btn btn-reset" value="reset">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
      $(document).ready(function(){
          var $btn_reset = $('[name="btn-password-reset"]');
          $btn_reset.click(function(ev){
              var $form = $('form');
              var $email = $form.find('[name="username"]').val();
              if ($email){
                  if (isValidEmail($email)){
                      var data = $form.serialize();
                      $.ajax({
                          url: '/user/reset',
                          dataType : 'json',
                          data: data,
                          type: 'POST'
                      })
                      .done(function(response){
                          if(response){
                             var code = response['code'] || 0;
                             var message = response['message'] || "";
                             var message_css = "text-success";
                              var html = "";
                             if(code === '200'){
                                 html += '<div class="'+message_css+'">';
                                 html += message;
                                 html += '</div>';
                                 html += '<div>';
                                 html += '<a href="/user/login">Follow this link for redirect to Login Page.</a>';
                                 html += '</div>';
                             }else{
                                 message_css = "text-error";
                                 html += '<div class="'+message_css+'">';
                                 html += message;
                                 html += '</div>';
                             }

                             $('[name="password-reset-message"]').empty().html(html)
                          }
                      })
                      .fail(function(jqXHR, textStatus){});

                      $form.find('[name="username-error"]').text("");
                  } else {
                      $form.find('[name="username-error"]').text('Invalid email format.')
                  }
              }else{
                  $form.find('[name="username-error"]').text('Can"t be empty!')
              }
          });
      });

    function isValidEmail(email){
        var result = false;
        var template = /^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$/;
        if(email.match(template)){
            result = true;
        }
        return result;
    }
  </script>
</body>
</html>
