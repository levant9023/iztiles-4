<%-- 
    Document   : right_block
    Created on : Apr 4, 2013, 1:05:22 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz"%>

<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page import="org.izsearch.controllers.SearchController"%>

<%long sc = SearchController.MAX_PICS_ON_MAIN;%>
<c:set var="max_images" value="<%=sc%>" />

<c:url var="images_url" value="/images.html">
  <c:param name="q" value="${requestScope.q}" />
</c:url>

<div id="mapblock">
  <%--<div class="images-map">
  <c:set var="imageList" value="${requestScope.images}" />
  <c:if test="${not empty imageList}">
    <ul class="images-list">
      <c:set var="count" value="1" />
      <c:forEach items="${imageList}" var="images" varStatus="images_status">
        <c:if test="${count le max_images}">
          <c:forEach items="${images.images}" var="image" varStatus="image_status">
            <li class="image-item">
              <%
                String img = (String)pageContext.getAttribute("image");
                byte[] img_url_b = Base64.encodeBase64(img.getBytes());
                pageContext.setAttribute("img_url", new String(img_url_b));
              %>
              <c:url var="images_url" value="/printout-images.html">
                <c:param name="q" value="${requestScope.q}" />
                <c:param name="offset" value="${requestScope.offset}" />
                <c:param name="records" value="${requestScope.records}" />
              </c:url>
              <a href="${images_url}&img=${img_url}">
                <img class="PreviewImg" src="${image}"/>
              </a>
            </li>
            <c:set var="count" value="${count + 1}" />
          </c:forEach>
        </c:if>
      </c:forEach>
    </ul>
  </c:if>
</div>--%>
</div>
