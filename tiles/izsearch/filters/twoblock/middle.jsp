<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/filters/images.css"/>

<c:set var="fullPath" value="${requestScope['javax.servlet.forward.request_uri']}"/>
<c:set var="domains" value="${requestScope['domains']}"/>
<c:set var="btn_flt_tool" value="${cookie['_dftbtl'].value}" scope="page"/>

<c:choose>
  <c:when test="${btn_flt_tool eq true}">
    <c:set var='visible' value='style="display: block"'/>
  </c:when>
  <c:otherwise>
    <c:set var='visible' value='style="display: none"'/>
  </c:otherwise>
</c:choose>

<div id="search-text">
  <div id="related-title">
    <div id="related-text"></div>
    <div id="also1" class="also"></div>
  </div>
  <div id="amazon">
    <div class="izslik"></div>
    <div class="ads-title">Ads by Amazon</div>
  </div>
  <div id="sec01" class="sec"></div>
  <div id="article-delimeter">
    <div id="end_search">End of the search results</div>
      <div class="loader" id="loader-2">
        <span></span>
        <span></span>
        <span></span>
      </div>
  </div>
  <div id="also2" class="also">
    <div id="tp_wrapper">
    <div id="top">
      <div class="ct"><h2>TOP</h2>
        <div class="info_icon top" data-toggle="popover" data-placement="right" data-content="Top news.">ⓘ</div>
      </div>
      <div class="izslik"></div>
    </div>
    <div class="breaking ct"><h2>LATEST NEWS</h2>
      <div class="info_icon" data-toggle="popover" data-placement="right" data-content="Breaking and most important news of Today.">ⓘ</div>
    </div>
    <div id="breaking" class="one_slider">
      <div class="izslik"></div>
    </div>
    <div id="more_breaking" class="hide_button">Show More</div>
    <div class="trending ct"><h2>TRENDING</h2>
      <div class="info_icon trend" data-toggle="popover" data-placement="right" data-content="">ⓘ</div>
    </div>
    <div id="trending" class="one_slider">
      <div class="izslik"></div>
    </div>
    <div id="more_trending" class="hide_button">Show More</div>
    <div class="popular ct"><h2>POPULAR</h2>
      <div class="info_icon pop" data-toggle="popover" data-placement="right" data-content="">ⓘ</div>
    </div>
    <div id="popular" class="one_slider">
      <div class="izslik"></div>
    </div>
    <div id="more_popular" class="hide_button">Show More</div>
    </div>
    <div class="pages ct"><h2>Sites</h2>
      <div class="info_icon pg" data-toggle="popover" data-placement="right" data-content="">ⓘ</div>
    </div>
    <div id="pages" class="one_slider">
      <div class="parent_div_for_faves faves_parent_1"></div>
      <div class="parent_div_for_faves faves_parent_2"></div>
      <div class="izslik"></div>
    </div>
    <div id="more_pages" class="hide_button">Show More</div>
  </div>
  <script>
    $(document).ready(function () {
      if(window.location.href.indexOf("food_recipes") > -1)
      {
        $( "#carousel" ).addClass( "visible" );
      }
      if(window.location.href.indexOf("images") > -1 || window.location.href.indexOf("video") > -1)
      {
        $( ".ct" ).css('display', "none");
      }
      $('.info_icon').popover({
        trigger: 'hover',
        placement: "auto right"
      });

      //Show-Hide trending/popular sliders

      $('body').on('click', '.hide_button', function(){
        $(this).prev().toggleClass('one_slider');
        $(this).html($(this).html() == 'Show More' ? 'Show Less' : 'Show More');
      });
    });
  </script>
  <div class="all ct"><h2>ALL</h2>
    <div class="event-motto"></div>
  </div>
    <div id="carousel">
      <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="5000">
        <div class="carousel-inner">
          <%--<div class="item active">
              <blockquote>
                  <div class="event-name">
                      <span>Super-quick <a href="${baseUrl}/food_recipes.html?q=soup">soups</a></span>
                  </div>
              </blockquote>
          </div>--%>
          <div class="item active">
            <blockquote>
              <div class="event-name">
                <span>Healthier <a href="${baseUrl}/food_recipes.html?q=casserole">casseroles</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Use Your <a href="${baseUrl}/food_recipes.html?q=Noodle">Noodle</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Make-ahead <a href="${baseUrl}/food_recipes.html?q=Lunch">lunches</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Ready, <a href="${baseUrl}/food_recipes.html?q=spaghetti">Spaghetti</a>, Go!</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span><a href="${baseUrl}/food_recipes.html?q=Meatless">Meatless</a> Meals</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Quick <a href="${baseUrl}/food_recipes.html?q=Chicken">chick recipes</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Quick, high <a href="${baseUrl}/food_recipes.html?q=protein">protein meals</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Easy Spooky <a href="${baseUrl}/food_recipes.html?q=Snacks">Snacks</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>5 Ingredient <a href="${baseUrl}/food_recipes.html?q=Meals">Meals</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span><a href="${baseUrl}/food_recipes.html?q=corn">A-maize-ing</a> recipes</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Killer <a href="${baseUrl}/food_recipes.html?q=Kale">Kale</a> Dishes</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Fresh <a href="${baseUrl}/food_recipes.html?q=seafood">Catch</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span><a href="${baseUrl}/food_recipes.html?q=protein">High protein snacks</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span><a href="${baseUrl}/food_recipes.html?q=seasonal">Seasonal Food</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span><a href="${baseUrl}/food_recipes.html?q=spring">Spring</a> meals ideas</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Veni, <a href="${baseUrl}/food_recipes.html?q=veggie">veggie</a>, vici!</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Seep your <a href="${baseUrl}/food_recipes.html?q=soup">soup</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Amen to <a href="${baseUrl}/food_recipes.html?q=ramen">ramen</a>!</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Feeling <a href="${baseUrl}/food_recipes.html?q=blood+orange">bloody</a>?</span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Babushka, balalaika, <a href="${baseUrl}/food_recipes.html?q=pelmeni">pelmeni</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Healthy <a href="${baseUrl}/food_recipes.html?q=avocado">Avocados</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Smoooooooth <a href="${baseUrl}/food_recipes.html?q=smoothie">smoothies</a></span>
              </div>
            </blockquote>
          </div>
          <div class="item">
            <blockquote>
              <div class="event-name">
                <span>Going back to the <a href="${baseUrl}/food_recipes.html?q=Root+Vegetables">roots</a></span>
              </div>
            </blockquote>
          </div>
        </div>
      </div>
    </div>
  <div id="sec02" class="sec"></div>
</div>
<div id="filter-images"></div>
