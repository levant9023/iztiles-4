<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>

<c:set var="fullPath" value="${baseURL}/${requestScope['javax.servlet.forward.request_uri']}" />
<c:set var="btn_sh" value="${cookie['btn_sh'].value}" scope="page"/>

<c:choose>
	<c:when test="${btn_sh eq 1}">
		<c:set var='visible' value='style="display: block"' />
	</c:when>
	<c:otherwise>
		<c:set var='visible' value='style="display: none"' />
	</c:otherwise>
</c:choose>

<div id="header-block">
	<div id="logo-block">
		<div id="logo">
			<a href="${baseURL}">
				<c:set var="logo" value="${iz:segments(fullPath)}" />
				<img src="${baseURL}/resources/img/logo.svg" alt="Logo: ${logo}">
			</a>
		</div>
	</div>

	<c:set var="segments" value="${iz:segments(fullPath)}" />
	<c:set var="uri" value="" />
	<c:if test="${not empty segments}">
		<c:set var="uri" value="${baseURL}/${segments[0]}.html" />
	</c:if>
	<div id="search-block">
    <div id="fpagetitle">
      <div id="fpagetitleimg">
				<a href="${baseURL}/${logo[0]}.html?q=">
					<img class="title_icon" src="/resources/img/nav/${logo[0]}.svg" />
					<img src="${baseURL}/resources/img/logos/titles/mobile/${logo[0]}.svg"/>
				</a>
			</div>
    </div>
    <div class="input_wrap">
      <form id="search-form" role="search" action="${uri}" method="GET" target="_self">
        <input id="input-search" class="input_trigger form-control hide_list" autocomplete="off" name="q" value="<c:out value='${param.q}' escapeXml='true'/>" autofocus>
        <button id="btn-search" type="submit" class="input-inside">
          <i class="glyphicon glyphicon-search"></i>
        </button>
        <div id="autosuggest" class="form-group">
          <ul id="autocomplete" class="autocomplete"></ul>
        </div>
      </form>
      <%-- Include Categorized Top Menu --%>
      <div class="query_hints">
        <div id="top_query" class="hide_list"></div>
        <div id="top_query_list"></div>
      </div>
    </div>
		<tiles:insertAttribute name="dropdown_filter"/>
		<div id="moreMenu" class="dropdown">
			<button id="moreMenuButton" class="btn btn-default dropdown-toggle" type="button" title="Discover business news, technical reviews, recipes, travel, style and other ideas to try." data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><span>Explore</span><img src="${baseURL}/resources/img/nav/menu.png" class="icon-inert"></button>
			<ul class="dropdown-menu" aria-labelledby="moreMenuButton">
				<span class="moreMenu_title">Explore Thematic Pages</span>
				<li><a href="/art.html"><img src="/resources/img/nav/art.svg"><span>Art</span></a></li>
				<li><a href="/blogs_magazines.html"><img src="/resources/img/nav/blogs_magazines.svg"><span>Blogs & Magazines</span></a></li>
				<li><a href="/books.html"><img src="/resources/img/nav/books.svg"><span>Books</span></a></li>
				<li><a href="/business.html"><img src="/resources/img/nav/business.svg"><span>Business</span></a></li>
				<li><a href="/cars.html"><img src="/resources/img/nav/cars.svg"><span>Cars</span></a></li>
				<li><a href="/celebrity.html"><img src="/resources/img/nav/celebrity.svg"><span>Celebrity</span></a></li>
				<li><a href="/craft.html"><img src="/resources/img/nav/craft.svg"><span>Craft</span></a></li>
				<li><a href="/design.html"><img src="/resources/img/nav/design.svg"><span>Design</span></a></li>
				<li><a href="/education.html"><img src="/resources/img/nav/education.svg"><span>Education</span></a></li>
				<li><a href="/family.html"><img src="/resources/img/nav/family.svg"><span>Family</span></a></li>
				<li><a href="/food_recipes.html"><img src="/resources/img/nav/food_recipes.svg"><span>Food & Recipes</span></a></li>
				<li><a href="/fun.html"><img src="/resources/img/nav/fun.svg"><span>Fun</span></a></li>
				<li><a href="/gifts.html"><img src="/resources/img/nav/gifts.svg"><span>Gifts</span></a></li>
				<li><a href="/health.html"><img src="/resources/img/nav/health.svg"><span>Health</span></a></li>
				<li><a href="/home.html"><img src="/resources/img/nav/home.svg"><span>Home & Garden</span></a></li>
				<li><a href="/jobs.html"><img src="/resources/img/nav/jobs.svg"><span>Jobs</span></a></li>
				<li><a href="/kids.html"><img src="/resources/img/nav/kids.svg"><span>Kids</span></a></li>
				<li><a href="/law.html"><img src="/resources/img/nav/law.svg"><span>Law</span></a></li>
				<li><a href="/lifestyle.html"><img src="/resources/img/nav/lifestyle.svg"><span>Lifestyle</span></a></li>
				<li><a href="/men.html"><img src="/resources/img/nav/men.svg"><span>Men</span></a></li>
				<li><a href="/movies.html"><img src="/resources/img/nav/movies.svg"><span>Movies</span></a></li>
				<li><a href="/music.html"><img src="/resources/img/nav/music.svg"><span>Music</span></a></li>
				<li><a href="/news.html"><img src="/resources/img/nav/news.svg"><span>News</span></a></li>
				<li><a href="/npo.html"><img src="/resources/img/nav/npo.svg"><span>NPO</span></a></li>
				<li><a href="/pets.html"><img src="/resources/img/nav/pets.svg"><span>Pets</span></a></li>
				<li><a href="/politics.html"><img src="/resources/img/nav/politics.svg"><span>Politics</span></a></li>
				<li><a href="/real_estate.html"><img src="/resources/img/nav/real_estate.svg"><span>Real Estate</span></a></li>
				<li><a href="/science.html"><img src="/resources/img/nav/science.svg"><span>Science</span></a></li>
				<li><a href="/seniors.html"><img src="/resources/img/nav/seniors.svg"><span>Seniors</span></a></li>
				<li><a href="/shopping.html"><img src="/resources/img/nav/shopping.svg"><span>Shopping</span></a></li>
				<li><a href="/sports.html"><img src="/resources/img/nav/sports.svg"><span>Sports</span></a></li>
				<li><a href="/style_fashion.html"><img src="/resources/img/nav/style_fashion.svg"><span>Style & Fashion</span></a></li>
				<li><a href="/teachers.html"><img src="/resources/img/nav/teachers.svg"><span>Teachers</span></a></li>
				<li><a href="/tech.html"><img src="/resources/img/nav/tech.svg"><span>Technology</span></a></li>
				<li><a href="/teens.html"><img src="/resources/img/nav/teens.svg"><span>Teens</span></a></li>
				<li><a href="/tips_tutorials.html"><img src="/resources/img/nav/tips_tutorials.svg"><span>Tips & Tutorials</span></a></li>
				<li><a href="/travel.html"><img src="/resources/img/nav/travel.svg"><span>Travel</span></a></li>
				<li><a href="/tools.html"><img src="/resources/img/nav/tools.svg"><span>Tools</span></a></li>
				<li><a href="/tv.html"><img src="/resources/img/nav/tv.svg"><span>TV</span></a></li>
				<li><a href="/women.html"><img src="/resources/img/nav/women.svg"><span>Women</span></a></li>
				<img class="arrow" src="/resources/img/nav/arrow.png">
			</ul>
		</div>
    <div id="right_block">
    </div>
		<%--<%@ include file="../../snippets/dropdown_top_menu.jsp" %>--%>
	</div>
	<div id="tools-block">
		<div class="collapse navbar-collapse" id="nvb-01">
			<ul id="search-toolbar" class="nav navbar-nav navbar-right">
				<li class="first" ${visible}>
					<div id="btg-bugfix" class="btn-group">
						<a id="remine-link" href="javascript:" class="btn" data-toggle="search-nav" role="popover" data-history="false" title="Bug report">
              <div id="btn-top-bugreport" class="btn-nav-img"></div>
						</a>

						<div class="popover-content hide">
							<div id="redmine-report">
								<div class="panel panel-default">
									<div class="panel-heading">
										<span class="simg glyphicon glyphicon-send"></span>
										<h3 class="bugfix-title">Report bugs</h3>
									</div>
									<div class="panel-body">
										<form id="form-redmine" class="izwidget" role="form">
											<div class="form-group">
												<label for="inputUserName" class="control-label">Your name (required)*:</label>
												<input type="text" class="form-control" id="inputUserName" name="username" value="">
												<span id="username_message" class="message"></span>
											</div>
											<div class="form-group">
												<label for="inputBugEmail" class="control-label">Your email (required)*:</label>
												<input type="email" class="form-control" id="inputBugEmail" name="usermail">
													<span id="usermail_message" class="message"></span>
											</div>
											<div class="form-group">
												<label for="inputBugSubject" class="control-label">Subject (required) *:</label>
												<input type="text" class="form-control" id="inputBugSubject" name="bugsubject">
												<span id="bugsubject_message" class="message"></span>
											</div>
											<div class="form-group">
												<label for="inputBugDescr" class="control-label">Bug description (required)*:</label>
												<textarea class="form-control" rows="2" id="inputBugDescr" name="bugdescr"></textarea>
												<span id="bugdescr_message" class="message"></span>
											</div>
											<div class="form-group">
												<div class="btn-send">
													<span id="redmine-result-message" class="message"></span>
													<button id="btn-redmine" type="button" class="btn btn-primary btn-block">
														<span class="glyphicon glyphicon-envelope left"></span>Send <span class="glyphicon glyphicon-arrow-right right"></span>
													</button>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="redmine-report-link" class="btn-group">
						<a id="redmine-blog" href="bugreport.html?page=1" class="btn" title="Bug report and Discussion Forum">
              <div id="btn-top-redmine" class="btn-nav-img"></div>
						</a>
					</div>
				</li>
				<li class="last">
					<div id="btg-sh" class="btn-group">
						<a href="javascript://" class="btn" title="Toolbar with extended option buttons" data-title="Toolbar with extended option buttons" data-toggle="tool-sh" role="popover">
              <div id="btn-top-tools" class="btn-nav-img"></div>
						</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#btn-trigger').click(function(){
			$('#btn-search').trigger('click')
		});
	})
</script>