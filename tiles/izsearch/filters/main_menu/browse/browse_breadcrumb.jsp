<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:if test="${not empty breadCrumb}">
  <nav class="favebreadcrumb">
    <a href="/browse.html">Browse</a><span class="delim"> |</span>
    <c:forEach var="category" items="${breadCrumb}" varStatus="loopStatus">
      <c:choose>
        <c:when test="${!loopStatus.last}">
          <a href="/browse.html?parentId=${category.id}">${category.title}</a><span class="delim"> |</span>
        </c:when>
        <c:otherwise>
          ${category.title}
          <c:set var="currentId" value="${category.id}"/>
        </c:otherwise>
      </c:choose>
    </c:forEach>
  </nav>
</c:if>