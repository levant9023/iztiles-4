<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/izsearch.tld" prefix="iz" %>


<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/izsearch/bugreport/bugreport.css"/>
<c:set value="${requestScope.issue}" var="issue"/>
<c:set value="${requestScope.posts}" var="posts"/>

<div id="bug-issue">
<div id="jimbotron">
  <div id="jmb-issue" class="panel panel-default" data-iid="<c:out value='${id}' default="" />">
    <div class="panel-title">
      <i class="fa fa-comment"></i>
      <span class="text"><c:out value="${issue['title']}" default=""/> </span>
    </div>
    <div class="panel-body">
      <div id="top-block">
        <span class="report-name left">Created by <c:out value="${issue['reporter']}" default=""/> </span>
        <span class="report-date">Reported at <c:out value="${issue['created']}" default="" /> </span>
      </div>
      <div id="middle-block"><c:out value="${iz:linkify(iz:url_decode(issue['descr']))}" default="" escapeXml="false"/></div>
    </div>
    <div class="panel-footer">
      <button id="btn-comments" class="btn btn-default" type="button">Comments&nbsp;<span class="badge"><c:out value="${fn:length(posts)}" default="0" /></span></button>
      <span class="buttom-block-responsible">responsible: <c:out value="${issue['assigned']}" default="" /></span>
      <span class="buttom-block-status right">status: <c:out value="${issue['status']}" default="" /> </span>
    </div>
  </div>
</div>

<div id="comments-panel" class="panel panel-default">
  <div id="comments-title" class="panel-title">
    <span class="ct">Comments</span>
    <button id="btn-new-comment" class="btn btn-success btn-sm right" type="button">
      <i class="fa fa-comments fa-lg"></i>
      New comment
    </button>
  </div>
  <div class="panel-body">
    <div id="comments-list">
      <ul id="jmb-comments" class="list-group">

        <c:forEach items="${posts}" varStatus="pst" var="post" >
          <li class="list-group-item">
            <div class="left-container">
              <i class="fa fa-user fa-lg"></i>
            </div>
            <div class="right-container">
              <div class="comment-header-item">
                <span class="poster left">posted by: <c:out value="${post['owner']}" default="" /></span>
                <span id="btn-comment-edit" class="edit"><i class="fa fa-edit"></i></span>
              </div>
              <pre class="comment-body-item"><c:out value="${iz:linkify(iz:url_decode(post['description']))}" default="" escapeXml="false"/></pre>
              <div class="comment-footer-item">
                <span class="comment-footer-item-state"><c:out value="${fn:substring(post['edited'], 0, 10)}" default="" /> </span>
              </div>
            </div>
          </li>
        </c:forEach>
      </ul>
      <div class="paginator"></div>
    </div>

    <div id="form-comment-container">
      <div id="form-title">
        <h4>Your comment</h4>
      </div>
      <div id="comments-form-panel">
        <form id="comment-form" role="form">
          <div class="form-group">
            <label for="inputUserName" class="control-label">Your name (required)*:</label>
            <input type="text" class="form-control input-sm" id="inputUserName" name="username" value="">
            <span id="username_message" class="message"></span>
          </div>
          <div class="form-group">
            <label for="inputComment" class="control-label">Comment (required)*:</label>
            <textarea class="form-control" rows="4" id="inputComment" name="comment" maxlength="4090"></textarea>
            <span id="comment_message" class="message"></span>
          </div>
          <div class="form-group">
            <div class="btn-send">
              <span id="redmine-result-message" class="message"></span>
              <button id="btn-post-comment" type="button" class="btn btn-sm btn-primary btn-block">Post</button>
            </div>
          </div>
          <input id="vfh" name="frmv" value="fisv">
        </form>
      </div>
    </div>
  </div>
</div>
</div>
