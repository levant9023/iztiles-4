<%-- 
    Document   : infoblock
    Created on : Dec 20, 2013, 1:28:14 AM
    Author     : work
--%>

<%@page import="org.izsearch.controllers.SearchController"%>
<%@page import="org.izsearch.model.LinkAndImages"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<%
    String basepath = request.getContextPath();
    double start = System.currentTimeMillis();
%>

<!-- Images block -->
<div class="imageblock">
    <%
    List<LinkAndImages> images = (List<LinkAndImages>) request.getAttribute("images");
    if (images != null && !images.isEmpty()) {%>
    <h3 class="underlinedh3">Images</h3>
    <%
        int i = 0;
        int img_size = 360 / SearchController.MAX_PICS_ON_MAIN;
        top:
        for (LinkAndImages li : images) {
            for (String image : li.getImages()) {
    %>
    <img class="PreviewImg" style="max-height: <%=img_size%>px;max-width: <%=img_size%>px;" src="<%=basepath + image%>"/>&nbsp;<%
                i++;
                if (i == SearchController.MAX_PICS_ON_MAIN) {
                    break top;
                }
            }
        }
    }
    %>
</div>