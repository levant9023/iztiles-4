<%--
  User: efanchik
  Date: 8/2/15
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="iz" uri="/WEB-INF/tlds/izsearch.tld" %>

<c:set var="query" value="${param.q}" />

<div class="bangs">
  <c:if test="${fn:length(requestScope.bangs) gt 0}">
    <ul class="bang-list">
    <c:forEach var="bang" items="${requestScope.bangs}" varStatus="status">
      <li class="bang-item" title="${bang.name}">
        <span class="img-span">
          <img src="/images/bangs/${bang.bang}.ico">
        </span>
        <span class="name">
          <a href="${iz:replace_text_holder(bang.url, query)}">${bang.name}(${bang.bang})</a>
        </span>
      </li>
    </c:forEach>
      </ul>
  </c:if>
</div>