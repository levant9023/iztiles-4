<%--
  User: efanchik
  Date: 8/2/15
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div id="report-widget" class="panel panel-default">
  <div class="panel-heading">
    <h1><span class="fa-img"><i class="fa fa-bar-chart"></i></span>Report</h1>
  </div>
  <div class="panel-body">
    <p class="total-query message">
      <span class="title">Total queries: </span>
      <span class="value"><c:out value="${report.totalQuery}" default="N/A" /></span>
    </p>
    <p class="total-ip message">
      <span class="title">Total ips: </span>
      <span class="value"><c:out value="${report.totalIp}" default="N/A" /></span>
    </p>
    <p class="unic-ip message">
      <span class="title">Unic ips: </span>
      <span class="value"><c:out value="${report.totalUnicIp}" default="N/A" /></span>
    </p>
    <p class="total-unic-ip message">
      <span class="title">Unic Ip List: </span>
      <c:set var="list_ip" value="${report.unicIp}" />
      <c:if test="${not empty list_ip}" >
        <div class="input-group" id="sel-ip-group">
          <select class="form-control input-sm" id="sel-ip">
            <option value="-1">Select ip from list</option>
            <c:forEach var="item" items="${list_ip}" varStatus="ipl" >
              <option value="${item}">${item}</option>
            </c:forEach>
          </select>
          <span class="input-group-btn">
           <button id="btn-whois" class="btn btn-sm btn-primary btn-sm">Renew</button>
          </span>
        </div>
      </c:if>
    </p>
    <div id="ipreport"></div>
  </div>
  <div class="panel-footer"></div>
</div>