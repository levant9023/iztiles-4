<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='more features' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <a href="${baseURL}"><img src="${baseURL}/resources/img/logo_search.svg" /></a>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Terms and Conditions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8 center">
      <ul class="arts list-unstyled">
        <li>
          <h3>T&C</h3>
          <p>This Agreement will enter into force from the moment of your first contact with the iZSearch.com and start using any of the services. You accept irrevocably and unconditionally and adhere to these Terms as you are accessing, browsing or using the Site and/or any of the Services. You also confirm that you agree to be bound by this Agreement without any exemptions, limitations and exclusions, and any and all provisions of this Agreement shall be enforceable to the fullest extent against you. You will accept the terms of this Agreement if you access the Site or use any of the Services on behalf a business. In this way iZSearch, Company, their officers, agents, employees and Partners were hold harmless and indemnify from any claim, suit or action arising from or related to the use of the Site and Services or violation of the Agreement, including any liability or expense arising from claims, losses, damages, suits, judgments, litigation costs and attorneys’ fees. The User thereby understands and agrees that  iZSearch  owns all copyrights, patent, trademark, know-how and other intellectual property rights (“IPR”) in relation to the Site.</p>
          <p>You should know that the sponsors or advertisers, who provide that content to iZSearch, have intellectual property rights to protect this content  presented to you, as part of the Site or Services. If you haven’t been specifically told by iZSearch or by the owners of that Content, in a separate agreement to modify, rent, lease, loan, sell, distribute or create derivative works based on this content (either in whole or in part), you may not do this.</p>
        <p>The right to review, filter, modify, refuse or remove any or all content from any Service is reserved by iZSearsh. For some of the Services, iZSearch may provide tools to filter out explicit sexual, obscene or indecent content. In addition, the access to material that you may find objectionable, may be limited by the commercially available services and software.</p>
          <p>Some of the Services may display advertisements and promotions, being supported by the advertising revenue. The content of information stored on the Services may be targeted to these advertisements, queries made through the Services or other information.</p>
          <p>The Services may include hyperlinks to other web sites or content or resources. These web sites or resources which are provided by companies or persons other than iZSearch, aren’t controlled by iZSearch.</p>
          <p>If you do not accept the Terms, you may not use the Site and Services.</p>
            <p>iZSrearch may modify these Terms, unilaterally without any notice.</p>
        </li>
      </ul>
    </div>
    <div class="col-md-2"></div>
  </div>

</div>