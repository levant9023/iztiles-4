<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>
	<head>
		<meta charset="UTF-8">
		<title>iZSearch search engine | Ads</title>
		<link rel="shortcut icon" type="image/png" href="${baseURL}/resources/img/favicon.png">
		<link rel="stylesheet" type="text/css" href="${baseURL}/resources/js/production/bootstrap/css/bootstrap.css"/>
		<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/mobile/api.css"/>
		<link type="text/css" rel="stylesheet" href="${baseURL}/resources/css/mobile/advertise_api.css"/>

		<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/bootstrap/js/can.min.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/mvc/classes.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/izsearch/subpages/advertise.js"></script>
		<script type="text/javascript" src="${baseURL}/resources/js/production/izsearch/subpages/adv_suggestion.js"></script>
	</head>
	<body>
		<div id="header">
			<div id="logo">
				<a class="logo" href="/"><img src="${baseURL}/resources/img/subpages/advertise.png" alt="logo"></a>
			</div>
		</div>
		<div id="banner-contain">
			<div id="banner-cont">
				<h1><span class="slogan"> iZ Ads - easy way to promote your business</span></h1>
				<p class="baner-txt">No headache about budget, no hustle with calculations, no hidden fees, no privacy issues!</p>

				<div class="hero-banner-btn">
					<a id="izads" href="${baseURL}/user/cart" class="btn--oxford btn--blue text--sm text--semilight">Buy iZAds</a>
					<a  href="${baseURL}/advertise_vertical.html" class="btn--oxford btn--blue text--sm text--semilight vertical_ads">iZAds for Vertical</a>
				</div>
			</div>
		</div>
		<div class="content">
			<h1><span>Why iZAds are so easy and straightforward?</span></h1>
			<div class="box1">
				<div class="box-col">
					<div class="img-col">
						<img src="${baseURL}/resources/img/about/advertise/adv1.jpg">
					</div>
					<div class="block_title"><span> Your Ad is the ONLY ONE on the desired page</span></div>
					<p>Per our philosophy - "one ad per page" - Your ad will be the only one on the page you desire. Simply, this page will "belong" to You, you can distribute and share it with your friends and partners and be sure it is always there.</p>
				</div>
				<div class="box-col">
					<div class="img-col">
						<img src="${baseURL}/resources/img/about/advertise/adv2.webp">
					</div>
					<div class="block_title"><span>You are 100% percent sure Your Ad is ALWAYS There</span></div>
					<p>Unlike other Ad platforms where you compete with others for the Ad space and never know when your Ad is shown, with iZAds You are the only one that will be advertised on "Your" page and You are 100% sure that your Ad is always there.</p>
				</div>
				<div class="box-col">
					<div class="img-col">
						<img src="${baseURL}/resources/img/about/advertise/adv3.jpg">
					</div>
					<div class="block_title"><span>No hidden fees. You know EXACTLY how much You pay</span></div>
					<p>You know EXACTLY how much You pay, for what period of time and what keywords at the moment of purchase. No hidden fees, no hidden calculations. You pay once and see the result!</p>
				</div>
				<div class="box-col">
					<div class="img-col">
						<img src="${baseURL}/resources/img/about/advertise/adv4.png">
					</div>
					<div class="block_title"><span>Buy MANY keywords for a FIXED PRICE</span></div>
					<p>Managing many keywords in your Ad Campaign is made easy. You can bulk upload many keywords from your file, instantly see your price and get a discount. You also get a discount for reserving a keyword for a longer period of time.</p>
				</div>
				<div class="box-col">
					<div class="img-col">
						<img src="${baseURL}/resources/img/about/advertise/adv5.jpg">
					</div>
					<div class="block_title"><span>PRIVACY- we do not use your keywords for anything else</span></div>
					<p>Per iZSearch philosophy we offer a search service which does not retain or share any of your personal information. We do not use your keywords or search data for any other purpose.</p>
				</div>
			</div>
		</div>

		<div class="container2">
			<h1><span>Get Started!</span></h1>
			<div class="list-cont">
				<ul>
					<li><span class="list-elements"><i class="glyphicon glyphicon-ok"></i> Search for Your Business keywords to see if they are available</span></li>
					<li><span class="list-elements"><i class="glyphicon glyphicon-ok"></i> Instantly see the price and how much You will pay, based on keywords popularity</span></li>
					<li><span class="list-elements"><i class="glyphicon glyphicon-ok"></i> Reserve MORE keywords and get a Discount</span></li>
					<li><span class="list-elements"><i class="glyphicon glyphicon-ok"></i> Reserve for a Longer Period of time and get a Discount</span></li>
				</ul>
			</div>

			<div id="advcalc" class="container-fluid">
				<div id="calc-title" class="ch1">
					<h1>Keyword Calculator</h1>
					<h4>Use this calculator to see the prices for the available keywords. Use check-boxes to see the total amount.</h4>
				</div>
				<div id="calc-frame" class="row">
					<div class="col-lg-3">
						<div id="adv-keys-form" class="form-group input-group-lg">
							<label class="control-label" for="input-keyword">Search your keywords here</label>
							<input class="form-control" id="input-keyword" name="keyword" type="text" value="running shoes"/>
							<div id="autosuggest" class="">
								<ul id="autocomplete" class="autocomplete"></ul>
							</div>
						</div>
						<div class="form-group search-group">
							<button id="searchKeywordsButton" class="btn btn-primary btn-md btn-block" name="submit">Search</button>
						</div>
					</div>
					<div class="col-lg-9">
						<table id="keywords-table" class="table table-hover">
							<thead>
								<tr><th>Keyword:</th><th>Price per year ($):</th></tr>
							</thead>
							<tbody></tbody>
							<tfoot>
								<tr id="tr-total">
									<td id="total-title" class="total-title">Total Amount (chose using check-boxes):</td>
									<td id="total-sum" class="total-sum"></td>
								</tr>
							</tfoot>
						</table>
						<div id="keywords-submit" class="form-group form-inline hide">
							<%--<button class="btn btn-primary btn-xs active" name="btn-kadd" type="button" id="add-keywords">Add</button>--%>
							<button class="btn btn-warning btn-xs" name="btn-kreset" type="button" id="reset-keywords">Reset</button>
						</div>
					</div>
				</div>
			</div>
			<div class="adv-buttons-wrapper">
				<a id="izads2" href="${baseURL}/user/cart" class="btn--oxford btn--blue text--sm text--semilight">Buy iZAds</a>
				<a  href="${baseURL}/advertise_vertical.html" class="btn--oxford btn--blue text--sm text--semilight vertical_ads">iZAds for Vertical</a>
			</div>
		</div>

		<footer class="bottom-fix">
			<div class="col-md-12 text-center">
				<span style="font-size: 18px;">&#169 iZSearch. Search it easy!</span>
			</div>
		</footer>
	</body>
</html>
