<%--
    Document   : more_features
    Created on : Oct 1, 2014, 11:15:04 PM
    Author     : efanchik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="t" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var='title' value='${requestScope.title}' scope="request"/>

<div class="container-fluid">
  <div id="board" class="row">
    <div id="top-nav" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center"></div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="logo" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <img src="${baseURL}/resources/img/logo_search.svg" alt="logo"/>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
    <div id="motto" class="row">
      <div class="col-md-4 text-left"></div>
      <div class="col-md-4 text-center">
        <h2>Questions</h2>
      </div>
      <div class="col-md-4 text-right"></div>
    </div>
  </div>
  <div id="arts" class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6 center">
      <h1 class="grob">iZSearch name</h1>
      <h3>iZSearch name</h3>
      <p>The name iZSearch is derived from Easy Search, but it's not a metaphor. If you're wondering how you would turn that into a verb... Search it easy!</p>
    </div>
    <div class="col-md-3"></div>
  </div>

</div>
