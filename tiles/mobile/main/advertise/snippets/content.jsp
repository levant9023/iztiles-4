<div id="companies" class="container">
	<h1>KEYWORDS SEARCH</h1>
	<div id="company-toolbar" class="row">
		<nav class="navbar navbar-default">
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><button id="btn-company-add" class="btn btn-default navbar-btn">Add New Campaign</button></li>
					<li><button id="btn-company-list" class="btn btn-default navbar-btn">My Campaigns</button></li>
				</ul>
			</div>
		</nav>
	</div>
	<div id="company-list" class="row"></div>
</div>
